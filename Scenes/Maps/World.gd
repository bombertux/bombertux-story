extends "res://Scenes/Maps/Map.gd"


func _ready():
	super._ready()
	
	$Areas/Main.connect("body_entered", func(body): self._area_entered($Areas/Main, body))
	$Areas/Room.connect("body_entered", func(body): self._area_entered($Areas/Room, body))
	$Areas/Room_right.connect("body_entered", func(body): self._area_entered($Areas/Room_right, body))
	$Areas/Room_bottom.connect("body_entered", func(body): self._area_entered($Areas/Room_bottom, body))
	
	self.adjust_camera_to_area($Player, $Areas/Room, false)
	
	for i in 6:
		var enemy = preload("res://Scenes/Entities/Enemies/Enemy.tscn").instantiate()
		enemy.position = Vector2(randi_range(3, 10), randi_range(3, 10)) * 16
		enemy.health = 4*(i+1)
		$Areas/Main/TileMap.add_child(enemy)


func _area_entered(source: Area2D, body: Node2D):
	var exited_area = Global.last_area
	
	source.get_node("TileMap").visible = true
	
	var player = body
	self.adjust_camera_to_area(player, source)
	
	if exited_area:
		exited_area.get_node("TileMap").process_mode = PROCESS_MODE_DISABLED
		
		var tween = get_tree().create_tween()
		tween.tween_property(
			player, "position", player.position + player.DIRECTION * 16, 1.25
		)
		tween.tween_callback(func():
			exited_area.get_node("TileMap").visible = false
		)
		
	source.get_node("TileMap").process_mode = PROCESS_MODE_INHERIT
	
	Global.last_area = source


func _area_exited(_source: Area2D, _body: Node2D):
	pass


func adjust_camera_to_area(player: Player, map_area: Area2D, animate: bool = true) -> Camera2D:
	var camera: Camera2D = player.get_node("Camera2D")
	var bounds: Rect2 = Utilities.get_collision_shape_bounds(map_area.get_node("CollisionShape2D"))
	#var enter_direction_vector = player.position - collision_shape.position
	
	var duration = 0
	if animate:
		duration = 1.25
		
	var tween = get_tree().create_tween().set_parallel().set_ease(Tween.EASE_OUT)
	
	#if enter_direction_vector.x > 0:
	tween.tween_property(camera, "limit_right", bounds.end.x,  duration)
	#elif enter_direction_vector.x < 0:
	tween.tween_property(camera, "limit_left", bounds.position.x,  duration)
	
	#if enter_direction_vector.y > 0:
	tween.tween_property(camera, "limit_bottom", bounds.end.y, duration)
	#elif enter_direction_vector.y < 0:
	tween.tween_property(camera, "limit_top", bounds.position.y,  duration)

	#if abs(localRect.size.x) >= abs(viewportRect.size.x):
	#camera.set_limit(SIDE_LEFT, localRect.position.x)
	#get_tree().create_tween().tween_property(camera, "limit_left", bounds.position.x,  duration).set_ease(Tween.EASE_OUT)
	#else:
		#camera.set_limit(SIDE_LEFT, -10000000)
	
	#if abs(localRect.size.y) >= abs(viewportRect.size.y):
	#camera.set_limit(SIDE_TOP, localRect.position.y)
	#get_tree().create_tween().tween_property(camera, "limit_top", bounds.position.y,  duration).set_ease(Tween.EASE_OUT)
	#else:
		#camera.set_limit(SIDE_TOP, -10000000)
	
	#if abs(localRect.size.x) >= abs(viewportRect.size.x):
	#camera.set_limit(SIDE_RIGHT, localRect.end.x)
	#get_tree().create_tween().tween_property(camera, "limit_right", bounds.end.x,  duration).set_ease(Tween.EASE_OUT)
	#else:
		#camera.set_limit(SIDE_RIGHT, 10000000)
	
	#if abs(localRect.size.y) >= abs(viewportRect.size.y):
	#camera.set_limit(SIDE_BOTTOM, localRect.end.y)
	#get_tree().create_tween().tween_property(camera, "limit_bottom", bounds.end.y, duration).set_ease(Tween.EASE_OUT)
	#else:
		#camera.set_limit(SIDE_BOTTOM, 10000000)
	
	return camera
