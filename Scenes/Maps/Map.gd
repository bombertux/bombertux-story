extends Node


func _ready():
	self.add_hud()
	self.add_menu()


func add_hud():
	var HUD = CanvasLayer.new()
	
	var PlayerHealth = preload("res://Scenes/UI/HealthBar.tscn").instantiate();
	HUD.add_child(PlayerHealth)
	PlayerHealth.connect_to_player(Global.player)
	
	add_child(HUD)


func add_menu():
	var MenuLayer = CanvasLayer.new()
	
	var Menu = preload("res://Scenes/UI/Menu.tscn").instantiate()
	MenuLayer.add_child(Menu)
	
	add_child(MenuLayer)
