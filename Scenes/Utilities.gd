extends Node


const TILE_SIZE = 16
const SCREEN_FADE_DURATION = 0.25


func get_level_position(scene) -> Vector2:
	return self.from_position_to_grid(scene.position)
	#var x = floor(scene.position.x / 16)
	#var y = floor(scene.position.y / 16)
	
	#return Vector2(x, y)


func get_level_position_grid(scene) -> Vector2:
	return self.from_grid_to_position(self.from_position_to_grid(scene.position))
	#return (self.get_level_position(scene) * self.TILE_SIZE) + Vector2(self.TILE_SIZE / 2, self.TILE_SIZE / 2)


func from_position_to_grid(position: Vector2) -> Vector2:
	return floor(position / self.TILE_SIZE)


func from_grid_to_position(grid_position: Vector2) -> Vector2:
	return (grid_position * self.TILE_SIZE) + Vector2(self.TILE_SIZE / 2.0, self.TILE_SIZE / 2.0)


func change_scene(scene):
	get_tree().root.add_child(scene)
	get_tree().current_scene.queue_free()
	get_tree().current_scene = scene


func change_scene_with_fade(scene):
	self.change_scene(scene)
	self.fade_screen(Color(0, 0, 0, 1), Color(0, 0, 0, 0))


func change_scene_with_player_to_position(
	scene: Node2D, player: Player, targetPlayerPositionNodePath: NodePath
):
	Global.player = player
	Global.last_area = null
	
	var targetPlayerPositionNode = scene.get_node(targetPlayerPositionNodePath)
	player.position = targetPlayerPositionNode.position
	scene.add_child(player)
	
	call_deferred("change_scene_with_fade", scene)


func fade_screen(from: Color, to: Color, duration: float = SCREEN_FADE_DURATION) -> Tween:
	var canvas = CanvasLayer.new()
	var rect = ColorRect.new()
	rect.color = from
	rect.size = get_viewport().get_visible_rect().size
	canvas.add_child(rect)
	get_tree().root.add_child(canvas)
	
	Global.player.set_process(false)
	var tween = get_tree().create_tween()
	tween.tween_property(rect, "color", to, duration)
	tween.tween_callback(func():
		get_tree().root.remove_child(canvas)
		Global.player.set_process(true)
	)
	
	return tween


func fade_bg_music(duration: float = SCREEN_FADE_DURATION):
	if get_tree().current_scene.has_node("MusicBackground"):
		var music = get_tree().current_scene.get_node("MusicBackground")
		var tween = get_tree().create_tween()
		tween.tween_property(
			music,
			"volume_db",
			ProjectSettings.get_setting("audio/buses/channel_disable_threshold_db"),
			duration
		)


func get_collision_shape_bounds(collision_shape: CollisionShape2D):
	var shape: Shape2D = collision_shape.shape
	var bounds
	
	if shape is RectangleShape2D:
		bounds = Rect2(
			collision_shape.position - shape.get_rect().size/2,
			shape.get_rect().size
		)
	
	return bounds


func add_collision_area(node: Node2D, original_collision_shape: Node2D):
	var collision_area = Collision.Area.new(node, original_collision_shape)
	
	# add area to node
	node.add_child(collision_area)
	
	return collision_area


class Collision:
	class Area extends Area2D:
		signal collided
		
		func _init(node: Node2D, original_collision_shape: Node2D, expand: bool = true):
			self.name = "CollisionArea"
			#super()
			
			# setup area
			self.collision_layer = node.collision_layer
			self.collision_mask = node.collision_mask
			
			for group in node.get_groups():
				self.add_to_group(group)
			
			self.connect("area_entered", func(area): emit_signal("collided", area))
			
			# copy shape and expand it
			var collision_shape = original_collision_shape.duplicate()
			if collision_shape is CollisionShape2D:
				var shape = collision_shape.shape.duplicate()
				
				if expand:
					if shape is RectangleShape2D:
						shape.set_size(shape.get_size() + Vector2(1, 1))
					elif shape is CapsuleShape2D:
						shape.set_height(shape.get_height() + 1)
						shape.set_radius(shape.get_radius() + 1)
					
				collision_shape.set_shape(shape)
				
			elif collision_shape is CollisionPolygon2D:
				var polygon = collision_shape.polygon.duplicate()
				
				if expand:
					pass
				
				collision_shape.set_polygon(polygon)
				pass
				
			self.add_child(collision_shape)
	
	
	class Layer:
		const PLAYER = 2
		const BOMB = 3
		const SOLID = 4
		const ENEMY = 5
		const EXPLOSION = 6
