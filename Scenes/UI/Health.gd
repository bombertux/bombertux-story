extends Control


@export var color_full: Color = Color(1, 0, 0, 1)
@export var color_empty: Color = Color(0, 0, 0, 1)
@export var color_extra_full: Color = Color(1, 1, 0, 1)
@export var color_extra_empty: Color = Color(0, 0, 0, 0)


func set_full():
	self.set_color_all(self.color_full)


func set_empty():
	self.set_color_all(self.color_empty)


func set_full_parts(parts: int):
	self.set_empty()
	for i in range(1, parts+1):
		self.set_color(i, self.color_full)


func set_extra_full():
	self.set_color_all(self.color_extra_full)


func set_extra_full_parts(parts: int):
	self.set_color_all(self.color_extra_empty)
	for i in range(1, parts+1):
		self.set_color(i, self.color_extra_full)


func set_color(index: int, color: Color):
	var color_rect: ColorRect = get_node("ColorRect" + str(index))
	color_rect.set_color(color)


func set_color_all(color: Color):
	for i in range(1, 4+1):
		self.set_color(i, color)
