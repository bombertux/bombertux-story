extends TabContainer


@onready var audio_bus = AudioServer.get_bus_index("Master")


func _ready():
	hide()
	
	self._ready_bombs()
	self._ready_system()


func _input(_event: InputEvent):
	if Input.is_action_just_pressed("ui_menu"):
		if get_tree().paused: # game is paused
			self.close_menu()
		else: # game is running
			self.open_menu()
	
	if Input.is_action_pressed("ui_menu_left"):
		if get_current_tab() > 0:
			set_current_tab(get_current_tab() - 1)
	if Input.is_action_pressed("ui_menu_right"):
		if get_current_tab() < get_tab_count() - 1:
			set_current_tab(get_current_tab() + 1)


func open_menu():
	get_tree().paused = true
	show()


func close_menu():
	hide()
	get_tree().paused = false


func get_player():
	return get_tree().get_current_scene().get_node("Player")


### Bombs ###


func _ready_bombs():
	for component in Bomb.COMPONENT_TYPE:
		var idx = Bomb.COMPONENT_TYPE[component]
		$Bombs/Panel/HBoxContainer/VBoxContainer3/BombComponents.set_item_text(idx, component.capitalize())


func _on_button_bomb_normal_pressed():
	self.get_player().Bomb = preload("res://Scenes/Entities/Bombs/Bomb__Normal.tscn")


func _on_button_bomb_breakables_pressed():
	self.get_player().Bomb = preload("res://Scenes/Entities/Bombs/Bomb__Breakables.tscn")


func _on_bomb_power_item_selected(index):
	var power = int($Bombs/Panel/HBoxContainer/VBoxContainer2/BombPower.get_item_text(index))
	self.get_player().bomb_power = power


func _on_bomb_components_multi_selected(_index, _selected):
	Global.player.bomb_components = []
	for idx in $Bombs/Panel/HBoxContainer/VBoxContainer3/BombComponents.get_selected_items():
		Global.player.bomb_components.append(idx) # same as enum idx


### System ###


func _ready_system():
	AudioServer.set_bus_volume_db(self.audio_bus, linear_to_db(0))
	$System/Panel/VBoxContainer2/SliderVolume.value = db_to_linear(AudioServer.get_bus_volume_db(self.audio_bus))


func _on_slider_volume_value_changed(value):
	AudioServer.set_bus_volume_db(self.audio_bus, linear_to_db(value))


func quit_game():
	get_tree().quit()


func _on_button_resume_pressed():
	self.close_menu()


func _on_button_quit_pressed():
	self.quit_game()
