extends Control


var Health = preload("res://Scenes/UI/Health.tscn")
var player: Player


func connect_to_player(player_to_connect: Player):
	self.player = player_to_connect
	self.draw_health()
	
	self.player.connect("health_changed", func (_health): self.draw_health())


func draw_health():
	for child in $HBoxContainer.get_children():
		child.queue_free()
	
	for i in int(player.health / 4.0):
		var healthNode = Health.instantiate()
		healthNode.set_full()
		$HBoxContainer.add_child(healthNode)
		
	if player.health % 4 > 0:
		var healthNode = Health.instantiate()
		healthNode.set_full_parts(player.health % 4)
		$HBoxContainer.add_child(healthNode)
	
	var health_diff = player.maxHealth - player.health
	for i in int(health_diff / 4.0):
		var healthNode = Health.instantiate()
		healthNode.set_empty()
		$HBoxContainer.add_child(healthNode)
	
	if player.extraHealth > 0:
		for i in int(player.extraHealth / 4.0):
			var healthNode = Health.instantiate()
			healthNode.set_extra_full()
			$HBoxContainer.add_child(healthNode)
		
		if player.extraHealth % 4 > 0:
			var healthNode = Health.instantiate()
			healthNode.set_extra_full_parts(player.extraHealth % 4)
			$HBoxContainer.add_child(healthNode)
