extends CharacterBody2D


const SPEED = 30

const DIRECTIONS = [Vector2.UP, Vector2.RIGHT, Vector2.DOWN, Vector2.LEFT]
var CURRENT_DIRECTION = Vector2.UP

@export var health: int = 4

var is_invincible = false


func _ready():
	add_to_group("enemies")
	
	var collision_area = Utilities.Collision.Area.new(self, $CollisionShape2D)
	collision_area.connect("collided", Callable(self, "_collide"))
	add_child(collision_area)
	
	$Label.text = str(self.health)


func _physics_process(delta):
	if CURRENT_DIRECTION == Vector2.UP:
		velocity.y -= SPEED
		$AnimatedSprite2D.play("up")
	elif CURRENT_DIRECTION == Vector2.DOWN:
		velocity.y += SPEED
		$AnimatedSprite2D.play("down")
	elif CURRENT_DIRECTION == Vector2.LEFT:
		velocity.x -= SPEED
		$AnimatedSprite2D.flip_h = true
		$AnimatedSprite2D.play("left")
	elif CURRENT_DIRECTION == Vector2.RIGHT:
		velocity.x += SPEED
		$AnimatedSprite2D.flip_h = false
		$AnimatedSprite2D.play("right")
	
	move_and_collide(velocity * delta)
	velocity = velocity.lerp(Vector2(0, 0), 1)


func take_damage(amount):
	if self.is_invincible:
		return
	
	self.set_invincibility()
	
	self.health -= amount
	
	if self.health == 0:
		queue_free()
	
	$Label.text = str(self.health)


func _collide(area: Area2D):
	if area.is_in_group("explosions"):
		self.take_damage(4)


func _on_movement_timer_timeout():
	var directions = self.DIRECTIONS.duplicate()
	
	directions.remove_at(directions.find(CURRENT_DIRECTION))
	directions.shuffle()
	
	CURRENT_DIRECTION = directions[0]
	
	$MovementTimer.start()


func set_invincibility():
	$InvincibilityTimer.start()
	$AnimatedSprite2D.set_modulate(Color(10, 1, 1, 1))
	self.is_invincible = true

func _on_invincibility_timer_timeout():
	$AnimatedSprite2D.set_modulate(Color(1, 1, 1, 1))
	self.is_invincible = false
