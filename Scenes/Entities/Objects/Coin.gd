extends StaticBody2D


func _ready():
	add_to_group("coins")
	
	var collision_area = Utilities.Collision.Area.new(self, $CollisionShape2D, false)
	collision_area.connect("collided", Callable(self, "_collide"))
	add_child(collision_area)


func hit_by_explosion():
	queue_free()


func _collide(area: Area2D):
	if area.is_in_group("player"):
		queue_free()
