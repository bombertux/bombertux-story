extends StaticBody2D


func _ready():
	var collision_area = Utilities.Collision.Area.new(self, $CollisionShape2D, false)
	collision_area.connect("collided", Callable(self, "_collide"))
	add_child(collision_area)


func hit_by_explosion():
	$AnimationPlayer.play("breaking")
	await $AnimationPlayer.animation_finished
	
	if randi_range(1, 3) == 1:
		call_deferred("spawn_coin")
	
	queue_free()


func spawn_coin():
	var coin = preload("res://Scenes/Entities/Objects/Coin.tscn").instantiate()
	coin.position = self.position
	get_tree().get_current_scene().add_child(coin)


func _collide(area: Area2D):
	if area.is_in_group("explosions"):
		self.hit_by_explosion()
