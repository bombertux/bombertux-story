extends Area2D


func _ready():
	add_to_group("explosions")
	
	$AnimatedSprite2D.play()


func _on_AnimatedSprite_animation_finished():
	queue_free()
