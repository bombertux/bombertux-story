extends Area2D


func _ready():
	self.add_area_edges()


func add_area_edges():
	var area_edges = StaticBody2D.new()
	area_edges.set_collision_layer_value(Utilities.Collision.Layer.SOLID, true)
	area_edges.add_collision_exception_with(Global.player)
	
	var bounds = Utilities.get_collision_shape_bounds($CollisionShape2D)
	var edges = [
		{ "a": bounds.position, "b": Vector2(bounds.end.x, bounds.position.y) },
		{ "a": bounds.position, "b": Vector2(bounds.position.x, bounds.end.y) },
		{ "a": bounds.end, "b": Vector2(bounds.end.x, bounds.position.y) },
		{ "a": bounds.end, "b": Vector2(bounds.position.x, bounds.end.y) },
	]
	
	for edge in edges:
		var collision_shape = CollisionShape2D.new()
		var shape = SegmentShape2D.new()
		shape.a = edge.a
		shape.b = edge.b
		collision_shape.shape = shape
		area_edges.add_child(collision_shape)
	
	add_child(area_edges)


func _on_body_entered(body):
	if body is Player:
		self._on_player_entered(body)


func _on_player_entered(player: Player):
	var exited_area = Global.last_area
	
	$TileMap.process_mode = PROCESS_MODE_INHERIT
	$TileMap.visible = true
	
	self.adjust_camera_to_area(player)
	
	if exited_area:
		exited_area.get_node("TileMap").process_mode = PROCESS_MODE_DISABLED
		
		var tween = get_tree().create_tween()
		tween.tween_property(
			player, "position", player.position + player.DIRECTION * 16, 1.25
		)
		tween.tween_callback(func():
			exited_area.get_node("TileMap").visible = false
		)
	
	Global.last_area = self


func adjust_camera_to_area(player: Player) -> Camera2D:
	var camera: Camera2D = player.get_node("Camera2D")
	var bounds: Rect2 = Utilities.get_collision_shape_bounds($CollisionShape2D)
	#var enter_direction_vector = player.position - collision_shape.position
	
	var duration = 0
	if Global.last_area:
		duration = 1.25
		
	var tween = get_tree().create_tween().set_parallel().set_ease(Tween.EASE_OUT)

	tween.tween_property(camera, "limit_top", bounds.position.y,  duration)
	tween.tween_property(camera, "limit_right", bounds.end.x,  duration)
	tween.tween_property(camera, "limit_bottom", bounds.end.y, duration)
	tween.tween_property(camera, "limit_left", bounds.position.x,  duration)
	
	return camera
