extends Area2D


@export_file() var sceneFilePath
@export var targetPlayerPositionNodePath: String

var scene: Node


func _ready():
	self.scene = load(self.sceneFilePath).instantiate()


func _on_body_entered(body: Node2D):
	if body.is_in_group("player"):
		var player = body.duplicate(DUPLICATE_USE_INSTANTIATION)
		body.process_mode = Node.PROCESS_MODE_DISABLED
		
#		var img_current = get_viewport().get_texture().get_image()
#		var viewport = get_viewport_rect()
#		img_current.resize(viewport.size.x, viewport.size.y)
#		$Sprite2D.texture = ImageTexture.create_from_image(img_current)
#
#		var camera: Camera2D = body.get_node("Camera2D")
#		$Sprite2D.global_position = camera.get_screen_center_position()
#
#		var tween = create_tween()
#		tween.tween_property($Sprite2D, "modulate", Color(0, 0, 0, 1), 0.75)
		var tween = Utilities.fade_screen(Color(0, 0, 0, 0), Color(0, 0, 0, 1), 0.75)
		Utilities.fade_bg_music(0.75)
		tween.connect("finished", func():
			Utilities.change_scene_with_player_to_position(
				self.scene, player, NodePath(targetPlayerPositionNodePath)
			)
		)


#func adjust_camera_to_tilemap(camera: Camera2D, tileMap: TileMap) -> Camera2D:
#	var tileMapRect = tileMap.get_used_rect()
#	var visibleCorrectionVector = Vector2(tileMap.cell_quadrant_size / 2.0, tileMap.cell_quadrant_size / 2.0)
#	var localRect = Rect2(
#		tileMap.map_to_local(tileMapRect.position) - visibleCorrectionVector,
#		tileMap.map_to_local(tileMapRect.size) - visibleCorrectionVector
#	)
#	var viewportRect = get_viewport_rect()
#
#	if abs(localRect.end.x) >= abs(viewportRect.end.x):
#		camera.set_limit(SIDE_LEFT, localRect.position.x)
#	else:
#		camera.set_limit(SIDE_LEFT, -10000000)
#
#	if abs(localRect.end.y) >= abs(viewportRect.end.y):
#		camera.set_limit(SIDE_TOP, localRect.position.y)
#	else:
#		camera.set_limit(SIDE_TOP, -10000000)
#
#	if abs(localRect.end.x) >= abs(viewportRect.end.x):
#		camera.set_limit(SIDE_RIGHT, localRect.end.x)
#	else:
#		camera.set_limit(SIDE_RIGHT, 10000000)
#
#	if abs(localRect.end.y) >= abs(viewportRect.end.y):
#		camera.set_limit(SIDE_BOTTOM, localRect.end.y)
#	else:
#		camera.set_limit(SIDE_BOTTOM, 10000000)
#
#	print(tileMapRect, localRect, viewportRect)
#
#	return camera
