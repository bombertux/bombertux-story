extends Node


func _ready():
	var scene = preload("res://Scenes/Maps/World.tscn").instantiate()
	var player = preload("res://Scenes/Entities/Player.tscn").instantiate()
	Utilities.change_scene_with_player_to_position(
		scene, player, NodePath("InitialPlayerPosition")
	)
