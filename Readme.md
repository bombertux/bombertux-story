## Concepts

### Bombs

BombComponents:
	- WaterComponent
	- BreakablePassThroughComponent
	- RemoteControlMoveComponent
	- RemoteDetonateComponent
	- (Kick & Throw?)
	- SaltComponent? (doesn't work in/under water)
Multiple attachable at once. upgrade bomb to attach more at once?

Adjustable fire power in menu.
Starts with power=2. gets more in adventure progress


### キャラボン

Use https://wiki.tuxemon.org/Category:Monster



## Ideas

Scene statt MapArea Transition:
Static screenshot von scene. on transition screenshot anlegen und verschieben animieren.
player schieben. wenn animation ende, neue scene als current_scene setzen



## Links

### Assets

- https://opengameart.org
- https://www.spriters-resource.com/game_boy_advance/bombertourn
